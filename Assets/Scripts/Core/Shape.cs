﻿using UnityEngine;
using System.Collections;

public class Shape : MonoBehaviour {

	public bool m_canRotate = true;
	//Used for shifting icon in queue
	public Vector3 m_queueOffset;

	//Particle Fx for the pieces of a shape
	GameObject[] m_glowSquareFx;
	public string m_glowSquareTag;
	public float instantDropSpeed = 1.001f;
	//Board board;

	void Start()
	{
		if(m_glowSquareTag != null && m_glowSquareTag != "")
		{
			m_glowSquareFx = GameObject.FindGameObjectsWithTag(m_glowSquareTag);
		}

		//board = GameObject.Find("Board").GetComponent<Board>();
	}

	public void LandShapeFX()
	{
		int i = 0;

		foreach(Transform child in gameObject.transform)
		{
			if(m_glowSquareFx[i])
			{
				m_glowSquareFx[i].transform.position = child.position;

				ParticlePlayer particlePlayer = m_glowSquareFx[i].GetComponent<ParticlePlayer>();
				if(particlePlayer)
				{
					particlePlayer.Play();
				}

				i++;

			}
		}
	}

	void Move(Vector3 moveDirection)
	{
		transform.position += moveDirection;
	}

	public void MoveLeft()
	{
		Move(new Vector3(-1,0,0));
	}
		
	public void MoveRight()
	{
		Move(new Vector3(1,0,0));
	}

	public void MoveDown(/* Only used for instant drop, optional*/float dropRate = 1.0f)
	{
		Move(new Vector3(0,-1 * dropRate,0));
	}

	public void MoveDownImmediately()
	{
		Move(new Vector3(0,-10,0));

	}

	public void MoveUp()
	{
		Move(new Vector3(0,1,0));
	}

	public void RotateRight()
	{
		if(m_canRotate)
		{
			transform.Rotate(0,0,-90);
		}
	}

	public void RotateLeft()
	{
		if(m_canRotate)
		{
			transform.Rotate(0,0,90);
		}
	}

	public void RotateClockwise(bool clockwise)
	{
		if(clockwise)
		{
			RotateRight();
		}
		else
		{
			RotateLeft();
		}
	}
}
